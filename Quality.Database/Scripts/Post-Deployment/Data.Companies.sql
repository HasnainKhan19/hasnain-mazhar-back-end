﻿
GO

PRINT N'Update default data in [dbo].[Companies]...';

GO

-- Companies
;MERGE INTO dbo.Companies AS Target
USING (
		select 1, 'ABC Manufacturing' union all
		select 2, 'Conecom Inc.' union all
		select 3, 'Acme Corporation'
) AS Source (Id, CompanyName)
	ON Target.Id = Source.Id
-- update matched rows
WHEN MATCHED THEN UPDATE 
	SET Target.CompanyName = Source.CompanyName
-- insert new rows
WHEN NOT MATCHED BY TARGET THEN	INSERT 
	VALUES (CompanyName);

GO