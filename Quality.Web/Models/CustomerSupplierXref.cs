﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class CustomerSupplierXref
    {
        [Key]
        [Column(Order = 0)]
        public int CustomerCompanyId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int SupplierCompanyId { get; set; }
        public string SupplierCodes { get; set; }
        public virtual Company CCID { get; set; }
        public virtual Company SCID { get; set; }
    }
}