﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quality.Web.Models
{
    public class Part
    {
        [Required]
        public int PartId { get; set; }
        [Required]
        public int CompanyId { get; set; }
        [Required]
        public string PartNumber { get; set; }
        public string PartName { get; set; }
        public string Jurisdiction { get; set; }
        public string Classification { get; set; }

        public virtual Company Company { get; set; }
    }
}