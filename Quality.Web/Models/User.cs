﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class User
    {
        public User()
        {
            CreatedBy = new HashSet<QualifiedPartsList>();
            LastUpdatedBy = new HashSet<QualifiedPartsList>();
        }
        [Required]
        public int ID { get; set; }
        public int CompanyId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string UserName { get; set; }

        public virtual Company Company { get; set; }

        //Qualified Parts List Table
        public virtual ICollection<QualifiedPartsList> CreatedBy { get; set; }
        public virtual ICollection<QualifiedPartsList> LastUpdatedBy { get; set; }
    }
}