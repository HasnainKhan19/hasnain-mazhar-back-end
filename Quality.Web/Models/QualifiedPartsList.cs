﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class QualifiedPartsList
    {
        public int Id { get; set; }
        [Required]
        public bool IsQualified { get; set; }
        public int PartId { get; set; }
        public string Revision { get; set; }
        [Required]
        public string ToolDieSetNumber { get; set; }
        public bool OpenPo { get; set; }
        public int CustomerCompanyId { get; set; }
        public int SupplierCompanyId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedDateUtc { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime LastUpdatedDateUtc { get; set; }
        public DateTime? ExpirationDateUtc { get; set; }
        [Required]
        public bool Ctq { get; set; }
        public virtual Company CCID { get; set; }
        public virtual Company SCID { get; set; }
        public virtual Part PID { get; set; }
        public virtual User CreatedBy { get; set; }
        public virtual User LastUpdatedBy { get; set; }
    }
}