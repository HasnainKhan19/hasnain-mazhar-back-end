﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    //https://msdn.microsoft.com/en-us/library/jj713564(v=vs.113).aspx

    public class Company
    {
        public Company()
        {
            CCID = new HashSet<CustomerSupplierXref>();
            SCID = new HashSet<CustomerSupplierXref>();
            QPLCCID = new HashSet<QualifiedPartsList>();
            QPLSCID = new HashSet<QualifiedPartsList>();
            PartCompanyID = new HashSet<Part>();
            UserCompanyID = new HashSet<User>();
        }
        [Required]
        public int Id { get; set; }
        [Required]
        public string CompanyName { get; set; }

        //CustomerSupplierXref table
        public virtual ICollection<CustomerSupplierXref> CCID { get; set; }
        public virtual ICollection<CustomerSupplierXref> SCID { get; set; }
        //QPL Table
        public virtual ICollection<QualifiedPartsList> QPLCCID { get; set; }
        public virtual ICollection<QualifiedPartsList> QPLSCID { get; set; }
        //parts Table
        public virtual ICollection<Part> PartCompanyID { get; set; }
        //User Table
        public virtual ICollection<User> UserCompanyID { get; set; }
    }
}