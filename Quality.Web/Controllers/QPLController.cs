﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace Quality.Web.Controllers
{
    public class QPLController : Controller
    {
        //GET QPL/Index
        string connectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=Quality.Database;Integrated Security=True";
        [HttpGet]
        public ActionResult Index()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter sqlData = new SqlDataAdapter("select Q.PartId, Q.Revision, P.PartName, " +
                    "Q.ToolDieSetNumber, Q.IsQualified, Q.OpenPo, P.Jurisdiction, P.Classification, " +
                    "C.CompanyName, Q.SupplierCompanyId, Q.Ctq, Q.LastUpdatedById, Q.LastUpdatedDateUtc" +
                    " from QualifiedPartsLists as Q, Parts as P, Companies as C " +
                    "Where Q.PartId = P.PartId AND Q.SupplierCompanyId = C.Id", conn);
                sqlData.Fill(dtbl);
            }
                return View(dtbl);
        }

        // GET: QPL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QPL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QPL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QPL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QPL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: QPL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QPL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
